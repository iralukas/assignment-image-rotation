#include "../include/bmp.h"
#include "../include/io.h"
#include <stdbool.h>

#define TYPE (19778)
#define BITCOUNT (24)
#define BISIZE (40)
#define BOFFBITS (54)

#pragma pack(push, 1)

struct bmp_header
{
    uint16_t bfType; 
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits; 
    uint32_t biSize;
    uint32_t biWidth; 
    uint32_t  biHeight;
    uint16_t  biPlanes; 
    uint16_t biBitCount; 
    uint32_t biCompression; 
    uint32_t biSizeImage; 
    uint32_t biXPelsPerMeter; 
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed; 
    uint32_t  biClrImportant;
};
#pragma pack(pop)



static uint64_t calculate_padding(uint64_t width){
    return (width % 4); 
}

struct bmp_header bmp_header_create(struct image const *img){
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = (BOFFBITS + img->height * img->width * sizeof(struct pixel) + img->height * calculate_padding(img->width)),
            .bOffBits = BOFFBITS,
            .biSize = BISIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biBitCount = BITCOUNT,
            .biSizeImage = img->height * img->width * sizeof(struct pixel) + calculate_padding(img->width) * img->height,
    };
}

static bool check_header(const struct bmp_header *h){
    return (h->bfType == TYPE && h->biBitCount == BITCOUNT);
}


enum bmp_read_status bmp_read(FILE * in, struct image * img){
    if(!in||!img) return READ_INVALID_PATH;

    struct bmp_header hdr = {0};
    if (fread(&hdr,sizeof(struct bmp_header),1,in)!=1) return READ_INVALID_HEADER;  // make function to read header?
    if (!check_header(&hdr)) return READ_INVALID_HEADER;

    struct image new = image_init(hdr.biWidth,hdr.biHeight);
    const uint8_t padding = calculate_padding(new.width);

    for (uint32_t i = 0; i <  hdr.biHeight ; i++){
        if (fread(&(new.data[i * new.width]), sizeof(struct pixel), new.width, in)!=new.width) return READ_INVALID_BITS; // read rows one by one
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_SIGNATURE;
    }
    if (!new.data) return READ_INVALID_BITS;
    *img = new;
    return READ_OK;

}

enum bmp_write_status bmp_write(FILE* out, struct image const* img){
    if (!out) return WRITE_ERROR;

    struct bmp_header hdr = bmp_header_create(img); 
    if (fwrite(&hdr, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;

    const char zero[] = {0, 0, 0}; 
    const uint8_t padding = calculate_padding(img->width); 

    for (uint64_t i = 0; i < img->height; i++) {  //write rows one by one
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out)!= img->width) return WRITE_ERROR;
        if (fwrite(zero, padding, 1, out)!= 1) return WRITE_ERROR;
    }
    return WRITE_OK;
}
