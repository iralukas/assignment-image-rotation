#pragma once

#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

struct bmp_header;
enum bmp_read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PATH,
    READ_ERROR,
};

enum bmp_write_status  {
    WRITE_OK,
    WRITE_ERROR,
};


enum bmp_read_status bmp_read( FILE * in, struct image* img );
enum bmp_write_status bmp_write( FILE* out, struct image const * img );

#endif
