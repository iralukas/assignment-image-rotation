#include "image.h"
#include <malloc.h>

struct image image_init(uint64_t width, uint64_t height){
    return (struct image){.width = width, .height = height, .data = malloc(height * width * sizeof(struct pixel))}; 
}

void image_free(struct image img){
    free(img.data);
}





