#ifndef LOGGER_H
#define LOGGER_H

#include "bmp.h"
#include "io.h"
#include <stdbool.h>

bool try_read_file(char const *filename, FILE **f);
bool try_write_file(char const *filename, FILE **f);
bool try_close_file(char const *filename, FILE **f);
bool try_read_bmp(char const *filename, struct image *img);
bool try_write_bmp(char const *filename, struct image *img);

#endif
