#include "../include/transform.h"
#include "../include/image.h"
#include <stdio.h>

struct image transform(struct image const img, struct image (f)(struct image const)) { 
    return f(img); 
}

static struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y) {
    return img.data[y * img.width + x];
}

static uint64_t calc_addr(const struct image p, const uint64_t x, const uint64_t y){
    return x * p.height + (p.height - 1 - y);
}

struct image rotate(struct image const p){
    struct image new = image_init(p.height,p.width);
    for (uint64_t y = 0; y < p.height; y++) { 
        for (uint64_t x = 0; x < p.width; x++) {
            new.data[calc_addr(p, x, y)] = get_pixel(p, x, y);
        }
    }
    return new;
}
