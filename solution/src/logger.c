#include "logger.h"
#include "bmp.h"
#include "io.h"

static const char* const open_file_message[] = {
    [FILE_OPEN_OK] = "File opened successfully\n",
    [FILE_WRITE_ERROR] = "Can't open file for writing\n",
    [FILE_READ_ERROR] = "Can't open file for reading\n"
};

static const char* const close_file_message[] = {
    [FILE_CLOSE_OK] = "File closed succesfully,",
    [FILE_CLOSE_ERROR] = "Can't close file\n",
};

static const char* const bmp_read_message[] = {
    [READ_OK] = "Bmp opened successfully\n",
    [READ_INVALID_SIGNATURE] = "Bmp invalid signature\n",
    [READ_INVALID_BITS] = "Bmp invalid data\n",
    [READ_INVALID_HEADER] = "Bmp invalid header\n",
    [READ_INVALID_PATH] = "Bmp invalid path\n",
    [READ_ERROR] = "Unknown bmp error\n",
};

static const char* const bmp_write_message[] = {
    [WRITE_OK] = "Bmp written successfully\n",
    [WRITE_ERROR] = "Can't write bmp\n",
};

bool try_read_file(char const *filename, FILE **f){
    enum open_file_status s = open_for_reading(f, filename);
    fprintf(stderr, "<%s>: %s", filename, open_file_message[s]);
    if (s != 0) printf("File error (%s): %s.\n", filename, open_file_message[s]);
    return s == 0;
}

bool try_write_file(char const *filename, FILE **f){
    enum open_file_status s = open_for_writing(f, filename);
    fprintf(stderr, "<%s>: %s", filename, open_file_message[s]);
    if (s != 0) printf("File error (%s): %s.\n", filename, open_file_message[s]);
    return s == 0;
}


bool try_close_file(char const *filename, FILE **f){
    enum close_file_status s = close_file(f);

    fprintf(stderr, "<%s>: %s", filename, close_file_message[s]);
    if (s != 0) printf("File error (%s): %s.\n", filename, close_file_message[s]);
    return s == 0;
}

bool try_read_bmp(char const *filename, struct image *img){
    FILE * in = {0}; 
    if (!try_read_file(filename, &in)) return 1;                       //check if we can read
    enum bmp_read_status s = bmp_read(in, img);                        // read bmp to img

    fprintf(stderr, "<%s>: %s", filename, bmp_read_message[s]);
    if (s != 0) printf("Bmp error (%s): %s.\n", filename, bmp_read_message[s]);
    if (!try_close_file(filename, &in)) return 1;                       // try to close the file
    return s == 0;
}

bool try_write_bmp(char const *filename, struct image *img){
    FILE * out = {0}; 
    if (!try_write_file(filename, &out)) return 1;                      // check if we can write
    enum bmp_write_status s = bmp_write(out, img); 

    fprintf(stderr, "<%s>: %s", filename, bmp_write_message[s]);
    if (s != 0) printf("Bmp error (%s): %s.\n", filename, bmp_write_message[s]);
    if (!try_close_file(filename, &out)) return 1;
    return s == 0;
}
