#include "../include/io.h"
#include <stdio.h>

enum open_file_status open_for_reading(FILE ** file, const char * path) {
    *file = fopen(path, "rb");
    if (file) return 0;
    else return FILE_READ_ERROR;
}

enum open_file_status open_for_writing(FILE ** file, const char * path){
    *file = fopen(path,"wb");
    if (file) return 0;
    else return FILE_WRITE_ERROR;
}

enum close_file_status close_file(FILE ** file){
    if (file) {
        fclose(*file);
        return 0;
    }
    else return FILE_CLOSE_ERROR;
}

