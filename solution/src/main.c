#include "../include/transform.h"
#include "../include/logger.h"
#include <malloc.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; 
    const char *path_from = argv[1];            
    const char *path_to = argv[2];
    struct image img = {0};

    if (!try_read_bmp(path_from, &img)) return 1;
    
    struct image new_img = transform(img, rotate);
    if (!try_write_bmp(path_to, &new_img)) return 1;

    image_free(img);
    image_free(new_img);

    return 0;
}
