#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct image transform(struct image const img, struct image (f)(struct image const));
struct image rotate(struct image const p);
#endif
