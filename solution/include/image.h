#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

struct image image_init(uint64_t width, uint64_t height);
void image_free(struct image img);
#endif
