#pragma once

#ifndef IO_H
#define IO_H

#include <stdio.h>

enum open_file_status {
    FILE_OPEN_OK = 0,
    FILE_WRITE_ERROR,
    FILE_READ_ERROR,
};

enum close_file_status{
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR,
};
enum open_file_status open_for_reading(FILE ** file, const char * path);
enum open_file_status open_for_writing(FILE ** file, const char * path);
enum close_file_status close_file(FILE ** file);

#endif
